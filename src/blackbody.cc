/*
    Copyright 2006-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Implementation of the blackbody class.

// $Id$

#include "blackbody.h"

/** Returns L_lambda at the specified wavelength in SI units. The
    calculation is based on an expression for (I_nu/Int I_nu dnu),
    which should be (L_nu/L). This was then transformed from _nu to
    _lambda. Lambda is in meters, luminosity in W and temp in K. */
mcrx::T_float mcrx::blackbody::emission(T_float lambda) const
{
  
  // The constant is 15 h^4 c^4/(pi^4 k^4)
  const T_float c = 6.59865838829e-9;
  // hc/k for the exp function, in SI units [m K].
  const T_float d = 1.43876866033e-2;

  return c*luminosity()/(pow(lambda,5)*pow(temperature_,4)*
			 (exp(d/(lambda*temperature()))-1));
}

/** Returns L_lambda, in SI units, for the specified wavelength
    vector. */
mcrx::array_1 mcrx::blackbody::emission(const array_1& lambda) const
{
  array_1 e(lambda.size());
  for(int i=0; i<lambda.size(); ++i)
    e(i) = emission(lambda(i));
  return e;
}
