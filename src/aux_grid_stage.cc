/*
    Copyright 2007-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Implementation and explicit instantiations of the aux_grid_stage
/// class. \ingroup mcrx

#include "config.h"
#include "mcrx-stage.h"
#include "mcrx-stage-impl.h"
#include "create_grid.h"
#include "emergence-fits.h"
#include "grid-fits.h"
#include "arepo_grid.h"
#include "arepo_emission.h"
#include "xfer_impl.h"


template <template <typename> class grid_type> 
void mcrx::aux_grid_stage<grid_type>::setup_objects ()
{
  // Build grid/emission (they are the same) and 
  cout << "Building grid" << endl;

  const int decomp_level = this->p_.getValue("domain_level", int(1), true);
  T_grid* dummy=0;
  this->g_ = create_grid(*this->input_file_, decomp_level, dummy);
  this->emi_ = this->g_;

  // Build cameras based on -PARAMETERS HDUs in output file
  cout << "Setting up emergence" << endl;
  this->eme_.reset(new T_emergence (*this->output_file_));

  // no dust model is used, this is just a dummy
  this->model_.reset(new T_dust_model());
}


template <template <typename> class grid_type> 
void mcrx::aux_grid_stage<grid_type>::load_file ()
{
  // first set up general stuff
  setup_objects();

  // Only thing to load is camera images
  this->eme_->load_images(*this->output_file_);
}


template <template <typename> class grid_type> 
void mcrx::aux_grid_stage<grid_type>::load_dump (binifstream& dump_file)
{
  // first set up general stuff
  setup_objects();

  // Only thing to load is camera images
  this->eme_->load_dump(dump_file);
}


template <template <typename> class grid_type> 
void mcrx::aux_grid_stage<grid_type>::save_file ()
{
  // Get normalization and save images
  this->eme_->write_images(*this->output_file_, 1.0, this->m_.units);
}


template <template <typename> class grid_type> 
void mcrx::aux_grid_stage<grid_type>::save_dump (binofstream& dump_file) const
{
  this->eme_->write_dump(dump_file);
}


template <template <typename> class grid_type> 
bool mcrx::aux_grid_stage<grid_type>::shoot ()
{
  return this->shoot_emission_integration ();
}

template <template <typename> class grid_type> 
void mcrx::aux_grid_stage<grid_type>::operator() ()
{
  // this is here so the function is instantiated
  this->run_stage();
}

// prevent instantiation of the xfer template in each stage. instead
// we do this in one place only
/*
extern template class mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
			   mcrx::dummy_grid, 
			   mcrx::mcrx_rng_policy>;
extern template class mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
			   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
					  mcrx::cumulative_sampling, 
					  mcrx::local_random>,
			   mcrx::mcrx_rng_policy>;
#ifdef WITH_AREPO
extern template class mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
			   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
					  mcrx::cumulative_sampling, 
					  mcrx::local_random>,
			   mcrx::mcrx_rng_policy>;
#endif
*/

// explicit instantiations of the aux_grid_stage class itself
template class mcrx::aux_grid_stage<mcrx::adaptive_grid>;

#ifdef WITH_AREPO
template class mcrx::aux_grid_stage<mcrx::arepo_grid>;
#endif
