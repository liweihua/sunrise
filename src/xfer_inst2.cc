/*
    Copyright 2007-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Explicit instantiations of the xfer classes for the Arepo grid and
/// for the generic dust model, aux_grid. The other xfer templates are
/// instantiated in xfer_inst.cc \ingroup mcrx

#include "config.h"
#include "mcrx.h"
#include "xfer_impl.h"
#include "arepo_grid.h"
#include "full_sed_grid.h"
#include "aux_grid.h"
#include "dust_model.h"
#include "scatterer.h"
#include "dummy.h"
#include "arepo_emission.h"
#include "mpi_master_impl.h"

// generic dust model, aux_grid
template class
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type, 
				    mcrx::mcrx_rng_policy>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random> >;
/*
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::xfer(T_grid&, T_emission&, T_emergence&, const T_dust_model&, T_rng_policy&, const T_dust_model::T_biaser&, T_float, T_float, T_float, T_float, bool, bool, bool, int,int);
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::xfer(const xfer&);
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::~xfer();
template void 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::shoot_isotropic();
template mcrx::mcrx_rng_policy&
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::rng();
template bool
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_adaptive_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::uses_local_emergence() const;
*/

#ifdef WITH_AREPO

// full instantiation of the dust_model, full_sed_grid<arepo_grid>
typedef  
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::T_full_sed_arepo_grid> T2;
template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::T_full_sed_arepo_grid>;
template void mcrx::mpi_master<T2>::thread_send_ray(const typename T2::T_queue_item&, int, int);

// generic dust model, aux_grid<arepo_grid>
/*
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::xfer(T_grid&, T_emission&, T_emergence&, const T_dust_model&, T_rng_policy&, const T_dust_model::T_biaser&, T_float, T_float, T_float, T_float, bool, bool, bool, int,int);
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::xfer(const xfer&);
template 
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::~xfer();
template void
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::shoot_isotropic();
template mcrx::mcrx_rng_policy&
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::rng();
template bool
mcrx::xfer<mcrx::generic_dust_model<mcrx::aux_pars_type>,
	   mcrx::aux_grid<mcrx::T_aux_arepo_grid,
			  mcrx::cumulative_sampling, 
			  mcrx::local_random>,
	   mcrx::mcrx_rng_policy>::uses_local_emergence() const;
*/

#endif
