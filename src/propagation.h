/*
  Copyright 2010 Patrik Jonsson, sunrise@familjenjonsson.org

  This file is part of Sunrise.

  Sunrise is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  Sunrise is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.
  
*/

/// \file propagation.h
/// Contains the ray_propagation_data class.
// $Id$

#ifndef __ray_propagation_data__
#define __ray_propagation_data__

#include "blitz-defines.h"
#include "mono_poly_abstract.h"

namespace mcrx {
  class ray_propagation_data;
};

/** This class is used to return the info about the ray propagation
    through the cell from the grid_cell class when propagating. */
class mcrx::ray_propagation_data {
public:
  /// The distance along the ray to the cell edge.
  T_float dl_; 
  /// The column density along the ray to the cell edge.
  T_densities dn_; 

  ray_propagation_data(T_float dl, T_densities dn):
    dl_(dl), dn_(independent_copy(dn)) {};

  template<typename T_expr>
  ray_propagation_data(T_float dl, const blitz::ETBase<T_expr>& dn) :
    dl_(dl), dn_(dn.unwrap()) {};

  ray_propagation_data() {};
};

#endif
