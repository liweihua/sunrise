/*
    Copyright 2007-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Implementation and explicit instantiations of the
/// ir_intensity_stage class. \ingroup mcrx

#include "config.h"
#include "mcrx-stage.h"
#include "mcrx-stage-impl.h"
#include "grid-fits.h"
#include "create_grid.h"
#include "equilibrium.h"
#include "arepo_grid.h"
#include "arepo_emission.h"

using namespace blitz;

template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::basic_setup ()
{
  if(this->n_rays_>0)
    cout << "Increasing the number of rays in the equilibrium calculation is not possible.\nCalculation will be restarted with the larger number of rays.\n";

  // read original and intensity wavelength vectors
  CCfits::ExtHDU& lambda_hdu = open_HDU (*this->output_file_, "LAMBDA");
  array_1 lambda_orig;
  read(lambda_hdu.column("lambda"), lambda_orig);
  read(lambda_hdu.column("lambda_intensity"), lambda_);
  int n_lambda_int;
  lambda_hdu.readKey("NLAMBDA_INTENSITY", n_lambda_int);
  lambda_.resizeAndPreserve(n_lambda_int);

  // Read dust grains
  cout << "Setting up dust model" << endl;
  typename T_dust_model::T_scatterer_vector 
    sv(read_dust_grains<typename T_dust_model::T_scatterer_vector>(this->p_, this->m_.units));
  this->model_.reset(new T_dust_model (sv));
  this->model_->set_wavelength(lambda_);

  // create shadow vector of grain_model* for temp calculation
  for(int i=0; i< this->model_->n_scatterers(); ++i) {
    grain_models_.push_back
      (dynamic_cast<grain_model<polychromatic_scatterer_policy, 
       mcrx_rng_policy>*>(&this->model_->get_scatterer(i)));
    grain_models_.back()->resample(lambda_);
    // trigger generation of interpolation table
    //grain_models_.back()->calculate_SED(lambda_,0);
  }

  // Now build grid
  cout << "Building grid" << endl;
  
  // The density generator is used to generate the densities of the
  // dust grains from the gas and metal densities in the simulation.  
  uniform_density_generator dg(this->p_);

  const int decomp_level = this->p_.getValue("domain_level", int(1), true);
  T_grid* dummy=0;
  std::pair<boost::shared_ptr<typename T_grid::T_grid_impl>,
	    boost::shared_ptr<T_grid> > grids
    (create_grid(*this->input_file_, dg, lambda_.size(), decomp_level, dummy));
  this->g_ = grids.second;
  
  // Create ir_grid emission object, supplying the adaptive_grid used
  // for the grid above.
  T_emgrid* dummy2=0;
  this->emgrid_ = create_grid(grids.first, *this->input_file_, 
			      dg, lambda_, dummy2);

  cout << "Setting up emission" << endl;

  CCfits::ExtHDU& pdata_hdu = open_HDU(*this->input_file_, "PARTICLEDATA");
  this->emi_.reset(new T_emission (pdata_hdu, this->g_.get(), 
				   lambda_orig, lambda_));

  // This stage uses an empty emergence, as we are only calculating intensities
  this->eme_.reset(new T_emergence ());

  // only load part of intensity that is for this task.
  const int nc = this->g_->n_cells();
  const int start_cell = mpi_calculate_offsets(nc);

  cout << "Allocating a " << star_intensity_.shape() 
       << " memory block for dust intensities, " 
       << product(star_intensity_.shape())*sizeof(T_float)*1.0/(1024*1024*1024)
       << " GB.\n";
  dust_intensity_.resize(nc,lambda_.size());
}

template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::setup_objects ()
{
  basic_setup();
}



/** Load IR stage data from a previously completed file. This is
     actually not supported. Because the temperature calculation have
     to be redone to convergence, there is very little lost by
     rerunning all rays. */
template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::load_file ()
{
  basic_setup();
}

/** Load optically thick IR stage data from a dump file.  */ 
template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::load_dump (binifstream& dump_file)
{
  basic_setup();

  // CHECK THAT THIS WORKS
  if (!this->m_.terminator()) {
    // Load dust temp distribution and cells remaining in calculation
    this->emgrid_->load_dump(dump_file);
    // load current grid intensities
    this->g_->load_dump(dump_file);

    // load dust_intensity
    TinyVector<int, 2> extent;
    dump_file >> extent [0]
	      >> extent [1];
    assert(all( dust_intensity_.shape() == extent));
    assert(dust_intensity_.isStorageContiguous());
    dump_file.read(reinterpret_cast<char*> (dust_intensity_.dataFirst()),
		   dust_intensity_.size()*sizeof (array_2::T_numtype) );
  }
}

template <template <typename> class grid_type> 
CCfits::ExtHDU* 
mcrx::ir_intensity_stage<grid_type>::create_hdus(blitz::TinyVector<int,2> size, 
						 const string& intunit)
{
  bool do_compress = this->p_.getValue("compress_images", false, true);
  const string i_name="DUSTINTENSITY";
  CCfits::ExtHDU* i_hdu;    
  try {
    i_hdu = &open_HDU (*this->output_file_, i_name);
  }
  catch (CCfits::FITS::NoSuchHDU&) {
    std::vector<long> naxes,znaxes;
    naxes.push_back(size[firstDim]);
    naxes.push_back(size[secondDim]);

    if (do_compress) {
      // use the cfitsio tile compression on these images
      znaxes=naxes;
      znaxes[0]=100; // compress 100 cells as one
      this->output_file_->setCompressionType(GZIP_1);
      this->output_file_->setTileDimensions(znaxes);
    }
    
    i_hdu = this->output_file_->addImage (i_name, FLOAT_IMG, naxes);
    this->output_file_->setCompressionType(0);
    
    i_hdu->writeComment("This HDU contains the mean radiation intensity in the grid cells due to dust emission. The first dimension is grid cell, the second wavelength.");

    i_hdu->addKey("imunit", intunit, "Mean intensity in units of "+intunit);
  }
  return i_hdu;
}


/** Save cell intensities.  */
template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::save_file ()
{
  const bool immediate_reemission=true;
  if(immediate_reemission) {
    // in this case the intensity IS in the grid
    const T_float normalization = 1.;
    this->g_->write_intensity(*this->output_file_, 
			      "INTENSITY","DEPOSITION",
			      1.0, *this->model_,
			      this->emi_->units().get("L_lambda"),
			      this->p_.getValue("compress_images", false, true) );
  }
  else {
  // Intensity is already normalized in equilibrium calculation. Note
  // that we want to save the dust_intensity_ array, not the intensity
  // in the grid object. The latter only includes the last iteration.
  CCfits::ExtHDU& intensity_hdu = open_HDU(*this->output_file_, "INTENSITY");
  string intunit;
  intensity_hdu.readKey("IMUNIT", intunit);

  std::cout << "Writing intensities" << std::endl;
  assert(dust_intensity_.size()>0);

  Array<float, 2> fintensity(dust_intensity_.shape());

  // handle zero-intensity case
  fintensity=where(dust_intensity_>0, 
		   log10(dust_intensity_), 
		   blitz::neghuge(float()));

  CCfits::ExtHDU* i_hdu;

  // this should probably be done by the grid class...
  if(this->g_->shared_domain()) {
    // shared domain. coadd arrays.
    mpi_sum_arrays(fintensity);
    if(is_mpi_master()) {
      i_hdu = create_hdus(fintensity.shape(), intunit);
      write(*i_hdu, fintensity);
      i_hdu->writeChecksum();
    }
  }
  else {    // split domain. Need to write consecutively
    Array<float, 2> out(mpi_stack_arrays(fintensity, firstDim));
    if(is_mpi_master()) {
      std::cout << "Writing intensities and energy deposition" << std::endl;
      i_hdu = create_hdus(out.shape(), intunit);
      write(*i_hdu, out);
      i_hdu->writeChecksum();
    }
  }
  }
}


/** Dump IR stage info. We might have been interrupted during the temp
    calculation, or we might have been interrupted during shooting. In
    either case, we should save the cell SEDs that have been
    calculated so we don't have to do it again. */
template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::save_dump (binofstream& dump_file) const
{
  this->g_->write_dump(dump_file);
}


template <template <typename> class grid_type> 
template<typename T>
void mcrx::ir_intensity_stage<grid_type>::calculate_dust_SED(const blitz::ETBase<T>& intensity)
{
  assert(0);
  mcrx_terminator term(this->m_);
  const int n_threads = this->p_.defined("n_threads")?
    this->p_.getValue("n_threads", int ()): 1;

  this->emgrid_->calculate_SED(intensity, grain_models_, term, n_threads, 0,
			       false);
}


template <template <typename> class grid_type> 
bool mcrx::ir_intensity_stage<grid_type>::shoot ()
{
  const T_float i_min = this->p_.getValue("i_min", T_float ());
  const T_float i_max = this->p_.getValue("i_max", T_float ());
  const T_float ir_tol = 
    this->p_.getValue("ir_equilibrium_tolerance", T_float());
  const T_float ir_lumfrac = 
    this->p_.getValue("ir_luminosity_percentile", T_float());
  const int n_f = this->p_.getValue("n_forced_ir", int(0), true);
  const int n_threads = this->p_.getValue("n_threads", int (1), true);
  assert (n_threads == this->m_.rng_states.size()); 
  const bool bind_threads = 
    this->p_.getValue("bind_threads", bool (false), true);
  const bool bank_size = this->p_.getValue("bank_size", int(1), true);

  mcrx_terminator term(this->m_);
  bool t;

  {
    // use weak references to avoid locking issues
    array_2 star_intensity_ref, dust_intensity_ref;
    star_intensity_ref.weakReference(star_intensity_);
    dust_intensity_ref.weakReference(dust_intensity_);
    
    t= determine_dust_equilibrium_intensities
      (
       *this->model_,
       *this->g_,
       *this->emgrid_,
       *this->emi_,
       lambda_,
       this->biaser(),
       dust_intensity_ref,
       this->m_.rng_states,
       this->n_rays_desired_,
       n_threads,
       bind_threads,
       bank_size,
       ir_tol,
       ir_lumfrac,
       i_min,
       i_max,
       term,
       false);
  }

  // set number of rays manually so it gets written out correctly
  this->n_rays_ = this->n_rays_desired_;
  // we can free the star intensity now
  star_intensity_.free();

  return t;
}

template <template <typename> class grid_type> 
void mcrx::ir_intensity_stage<grid_type>::operator() ()
{
  // this is here so the function is instantiated
  this->run_stage();
}



// prevent instantiation of the xfer template in each stage. instead
// we do this in one place only
extern template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid>;
extern template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::T_full_sed_adaptive_grid>;
#ifdef WITH_AREPO
extern template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::T_full_sed_arepo_grid>;
#endif

// explicit instantiations
template class mcrx::ir_intensity_stage<mcrx::adaptive_grid>;

#ifdef WITH_AREPO
template class mcrx::ir_intensity_stage<mcrx::arepo_grid>;
#endif
