/*
    Copyright 2007-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Implementation and explicit instantiations of the thick_ir_stage
/// class. \ingroup mcrx

#include "config.h"
#include "mcrx-stage.h"
#include "mcrx-stage-impl.h"
#include "create_grid.h"
#include "arepo_grid.h"
#include "arepo_emission.h"

using namespace std;


template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::basic_setup ()
{
  // read original and intensity wavelength vectors
  CCfits::ExtHDU& lambda_hdu = open_HDU (*this->output_file_, "LAMBDA");
  read(lambda_hdu.column("lambda"), elambda_);
  read(lambda_hdu.column("lambda_intensity"), ilambda_);
  int n_lambda_int;
  lambda_hdu.readKey("NLAMBDA_INTENSITY", n_lambda_int);
  ilambda_.resizeAndPreserve(n_lambda_int);

  // Read dust grains
  cout << "Setting up dust model" << endl;
  typename T_dust_model::T_scatterer_vector sv(read_dust_grains<typename T_dust_model::T_scatterer_vector>(this->p_, this->m_.units));
  this->model_.reset(new T_dust_model (sv));
  this->model_->set_wavelength(elambda_);

  // create shadow vector of grain_model* for temp calculation
  for(int i=0; i< this->model_->n_scatterers(); ++i) {
    grain_models_.push_back
      (dynamic_cast<grain_model<polychromatic_scatterer_policy, 
       mcrx_rng_policy>*>(&this->model_->get_scatterer(i)));
    grain_models_.back()->resample(ilambda_, elambda_);
  }

  // Now build grid
  cout << "Building grid" << endl;
  
  // The density generator is used to generate the densities of the
  // dust grains from the gas and metal densities in the simulation.  
  uniform_density_generator dg(this->p_);

  // we don't track intensities in this stage, so we can set n_lambda
  // to zero to save memory here.
  const int decomp_level = this->p_.getValue("domain_level", int(1), true);
  const int n_lambda = 0; 
  T_grid* dummy=0;
  std::pair<boost::shared_ptr<typename T_grid::T_grid_impl>,
	    boost::shared_ptr<T_grid> > grids
    (create_grid(*this->input_file_, dg, n_lambda, decomp_level, dummy));
  this->g_ = grids.second;

  // Create ir_grid emission object, supplying the adaptive grid used
  // for the grid above
  cout << "Setting up emission" << endl;

  // For the emission object we do however need to supply the lambda vector.
  T_emission* dummy2=0;
  this->emi_ = create_grid(grids.first, *this->input_file_, dg, 
			   elambda_, dummy2);

  // set up cameras
  this->eme_.reset(new T_emergence (*this->output_file_));

  // if we are doing kinematic calculation, set the wavelength scale
  // of the cameras
  if(this->p_.getValue("use_kinematics", false, true)) {
    typename T_emergence::iterator stop= this->eme_->end();
    for (typename T_emergence::iterator c = this->eme_->begin();
	 c != stop; ++c)
      (*c)->set_dopplerscale(elambda_);
  }

  // tell cameras to allocate
  typename T_emergence::iterator stop= this->eme_->end();
  for (typename T_emergence::iterator c = this->eme_->begin();
       c != stop; ++c) {
    (*c)->allocate(elambda_);
    assert((*c)->get_image().size()>0);
  }

  // read intensity data.
  {
    cout << "Reading radiation intensities" << endl;

    // figure out where "our" part of the array starts
    const int nc = this->emi_->n_cells();
    const int start_cell = mpi_calculate_offsets(nc);
    try {
      CCfits::ExtHDU& intensity_hdu = open_HDU(*this->output_file_, "INTENSITY");
      read(intensity_hdu, intensity_, nc, start_cell);
      cout << "Allocating a " << intensity_.shape() 
	   << " memory block for radiation intensities," 
	   << product(intensity_.shape())*sizeof(T_float)*1.0/(1024*1024*1024)
	   << " GB.\n";
      intensity_= pow (10., intensity_);
    }
    catch (CCfits::FITS::NoSuchHDU&) {
      cerr << "Error: No Radiation intensity data (INTENSITY HDU) found\n";
      throw;
    }
  }
}

template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::setup_objects ()
{
  basic_setup();
}



/** Load IR stage data from a previously completed file. This is
     actually not supported. Because the temperature calculation have
     to be redone to convergence, there is very little lost by
     rerunning all rays. */
template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::load_file ()
{
  basic_setup();
}

/** Load optically thick IR stage data from a dump file.  */ 
template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::load_dump (binifstream& dump_file)
{
  basic_setup();

  if (!this->m_.terminator()) {
    // load current images
    this->eme_->load_dump(dump_file);
  }
}


/** Save images of IR emission.  */
template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::save_file ()
{
  this->eme_->write_images(*this->output_file_, 1, this->m_.units,
			   "-"+stage_ID(),
			   this->p_.getValue("compress_images", false, true),
			   this->p_.getValue("write_images_parallel", false, true));

  // Save cell emission SEDs if requested
  if(this->p_.getValue("save_cell_seds", false, true))
    this->emi_->write_seds(*this->output_file_, "CELLSEDS", true);

  // Save dust temps if requested
  if(this->p_.getValue("save_dust_temps", false, true))
    this->emi_->write_temps(*this->output_file_, "DUSTTEMPS");
}


/** Dump IR stage info. We might have been interrupted during the temp
    calculation, or we might have been interrupted during shooting. In
    either case, we should save the cell SEDs that have been
    calculated so we don't have to do it again. */
template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::save_dump (binofstream& dump_file) const
{
  this->eme_->write_dump(dump_file);
}


template <template <typename> class grid_type> 
template<typename T>
void mcrx::thick_ir_stage<grid_type>::calculate_dust_SED(const blitz::ETBase<T>& intensity)
{
  mcrx_terminator term(this->m_);
  const int n_threads = this->p_.defined("n_threads")?
    this->p_.getValue("n_threads", int ()): 1;

  this->emi_->calculate_SED(intensity, grain_models_, term, n_threads, 0,
			    false, false, 
			    this->p_.getValue("save_dust_temps", false, true));
  cout << "Dust luminosity is " << this->emi_->total_luminosity() << endl;
}


template <template <typename> class grid_type> 
bool mcrx::thick_ir_stage<grid_type>::shoot ()
{
  mcrx_terminator term(this->m_);

  const int minscat = this->p_.getValue("n_scatter_min", int(0), true);

  {
    // use weak references to avoid locking issues
    array_2 intensity_ref;
    intensity_ref.weakReference(intensity_);

    calculate_dust_SED(intensity_ref);
  }

  // at this point, we no longer need the intensity array, so we can free it
  intensity_.free();

  // start by doing direct integration, if asked for
  if(this->p_.getValue("integrate_ir", true, true) &&
     (minscat==0)) {
    cout << "Integrating source function for direct dust emission\n";

    this->shoot_integration();

    // set minscat to 1 so we only get scattered light when we shoot next
    this->p_.setValue("n_scatter_min", int(1));
  }

  this->emi_->normalize_for_sampling();

  //
  // we don't need to track intensities now, this is purely for
  // propagating to cameras, so add_intensity is false.
  this->m_.mon_.set_images(*this->eme_);
  bool t = this->shoot_scatter (false);
  this->m_.mon_.clear_images();


  this->p_.setValue("n_scatter_min", minscat);
  this->p_.mark_used("n_scatter_min");

  return t;
}


template <template <typename> class grid_type> 
void mcrx::thick_ir_stage<grid_type>::operator() ()
{
  // this is here so the function is instantiated
  this->run_stage();
}


// prevent instantiation of the xfer template in each stage. instead
// we do this in one place only
extern template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::dummy_grid>;
extern template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::T_full_sed_adaptive_grid>;
#ifdef WITH_AREPO
extern template class 
mcrx::xfer<mcrx::dust_model<mcrx::scatterer<mcrx::polychromatic_scatterer_policy,
					    mcrx::mcrx_rng_policy>,
			    mcrx::cumulative_sampling,
			    mcrx::mcrx_rng_policy>,
	   mcrx::T_full_sed_arepo_grid>;
#endif

// explicit instantiations
template class mcrx::thick_ir_stage<mcrx::adaptive_grid>;

#ifdef WITH_AREPO
template class mcrx::thick_ir_stage<mcrx::arepo_grid>;
#endif
