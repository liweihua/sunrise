/*
    Copyright 2008-2011 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Contains functions that operate on units.

// $Id$

#include "mcrx-units.h"
#include <cassert>
#include <boost/lexical_cast.hpp>
#include <stdexcept>

using namespace std;
using namespace boost;

bool mcrx::check_consistency (const T_unit_map& a, const T_unit_map& b,
			      bool exit_on_fail)
{
  bool fail = false;
  string error;
  for (T_unit_map::const_iterator i = a.begin(); i != a.end(); ++i) {
    T_unit_map::const_iterator j = b.find(i->first );
    if (j != b.end()) {
      // this qty is defined in both maps, check it
      assert (i->first == j->first);
      if (i->second != j->second) {
	error= "Warning: Inconsistent units for "+lexical_cast<string>(i->first)+": "
	  +lexical_cast<string>(i->second) + " and " +
	  lexical_cast<string>(j->second);
	cerr << error << endl;
	fail = true;
      }
    }
  }
  if (fail && exit_on_fail)
    throw runtime_error(error);

  return fail;
}

