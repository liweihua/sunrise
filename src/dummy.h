/*
    Copyright 2007-2012 Patrik Jonsson, sunrise@familjenjonsson.org

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/** \file 

    Declaration of dummy classes that are used to make non-used
    functionality compile. */

#ifndef __dummy__
#define __dummy__


namespace mcrx {
  class dummy_cell;
  class dummy_grid;
  
  template<typename> class generic_biaser;
  template<typename> class ray;
  template<typename, typename> class dummy_scatterer;
  template<typename> class dummy_emitter;
};

class mcrx::dummy_cell {
public:
  class cell_tracker {
  public:
    typedef int T_code;
    template<typename T_arch> void serialize(T_arch& ar, unsigned int version) {};
    void* data() {return 0; };
    T_code code() const { return 0; };
  };
};

class mcrx::dummy_grid {
public:
  typedef dummy_cell T_cell;
  typedef dummy_cell::cell_tracker T_cell_tracker;
  typedef void T_data;
  typedef char T_location_request;
  typedef char T_location_response;

  std::vector<bool> position_ownership(std::vector<vec3d>) const {
    assert(0); return std::vector<bool>(); };
};

template <typename content_type, typename rng_policy_type>
class mcrx::dummy_scatterer {
public:
  typedef content_type T_lambda;
  typedef rng_policy_type T_rng_policy;
  typedef generic_biaser<T_lambda> T_biaser;
  typedef ray<T_lambda> T_ray;

  T_lambda opacity() const {assert(0); return T_lambda(0.0);};
  T_lambda albedo() const {assert(0); return T_lambda(0.0);};

  virtual const angular_distribution<T_lambda>& phase_function () const {
    assert(0); angular_distribution<T_lambda>* dummy(0); return *dummy; };
  virtual const angular_distribution<T_lambda>& 
  scatter (T_biaser b, T_ray& r, T_rng_policy& rng, T_float& prob) const {
    assert(0); angular_distribution<T_lambda>* dummy(0); return *dummy; };
};

template<typename lambda_type>
class mcrx::dummy_emitter {
public:
  lambda_type& get_emission() const { 
    assert(0); lambda_type* dummy(0); return *dummy; };
  template<typename rng_policy_type>
  void effective_scatter(ray<lambda_type>&, const lambda_type&, 
			 const rng_policy_type&) const {};
};

#endif
