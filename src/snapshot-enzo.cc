/*
    Copyright 2009-2011 Patrik Jonsson, (sunrise@familjenjonsson.org).

    This file is part of Sunrise.

    Sunrise is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    Sunrise is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sunrise.  If not, see <http://www.gnu.org/licenses/>.

*/

/// \file
/// Functionality for reading fits files converted with YT. \ingroup sfrhist

#include <iostream>
#include "snapshot.h"
#include "CCfits/CCfits"
#include "fits-utilities.h"

using namespace std;
using namespace CCfits;

/** Loads particles in PARTICLEDATA HDU into new star particles. The
    name of this function is really a misnomer, it loads data exported
    from Yt, which can come from Enzo but also from any other code Yt
    can read. */
bool mcrx::Snapshot::loadenzo(const string& snapfn)
{
  cout << "Reading Yt fits file: " << snapfn << endl;

  FITS input_file(snapfn, Read);

  name_ = snapfn;
  // yt snapshots have always been converted to physical units
  a_=1;
  // and are always in kpc, msun, yr
  lengthunit_=1;
  massunit_=1;
  timeunit_=1;

  /// metadata are in YT HDU \todo these should be propagated to the
  /// output file
  ExtHDU& yt_hdu = open_HDU(input_file, "YT");
  yt_hdu.readKey("snaptime", time_);

  ExtHDU& data_hdu = open_HDU(input_file, "PARTICLEDATA");
  nstar_particles.load_particledata(data_hdu);
  // shouldn't do anything but called for consistency.
  makephysicalunits();
  return true;
}

