Broadband is the program that integrates the data cubes output by mcrx
into broadband filter images.  It takes the mcrx output file and
produces a file containing images of the object in all bands desired,
with and without dust.

Broadband takes a configuration filename as its only argument.  The
configuration file consists of keyword-value pairs which are used to
control the behavior of the program.

The required keywords are:
input_file	mcrx_000.fits
		The input file (created by mcrx).
output_file	broadband_000.fits
		The name of the output FITS file where the results are
		saved.
filter_list	filters
		The name of the file containing the names of the
		filters for which the broadband images are to
		be calculated.
filter_file_directory	~/dust_data/filters/
			The directory where the filter bandpass files
			can be found.
Filter_lambda_conversion    1e-10
			    The conversion factor used to convert the
			    wavelengths in the filter files to the
			    wavelengths of the SED.  (The example is
			    appropriate for when filter files are in
			    Angstrom and the SED is in meters.)


Optional keywords are:
use_counters	false
		Specifies whether counters should print to standard
		out.  This is nice if you are running interactively,
		but makes log files messy for batch jobs.  Default is
		true. 
use_hpm		true
		Specifies whether the HPM Toolkit should be used for
		performance analysis.  Default is false.
CCfits_verbose	true
		Specifies whether CCfits should be in verbose mode,
		which will print diagnostic messages if there's a
		problem.  However, it will also print messages that
		look suspicious but are normal, so the default is
		false.

The output file is a FITS file.  In order to propagate all the
necessary input information, it contains a copy of all the HDU's in
the input grid file except the "CAMERAi",
"CAMERAi-SCATTER/NONSCATTER/ATTENUATION", "GRIDSTRUCTURE", "LAMBDA",
"DEPOSITION","RANDOM_STATE" and "INTEGRATED_QUANTITIES" HDU's.  The
pHDU has the following keywords set:

FILETYPE    "BROADBAND" 
	    The file contains broadband images.
DATATYPE    "GRID"
	    The data is on a grid (as opposed to a particle set).
FILTERCODE    "BROADBAND"
	    Specifies the code which was used to calculate the
	    broadband images, as well as the HDU in which the
	    configuration keywords can be found.
	    Currently, only BROADBAND is used here.

HDU "BROADBAND" (or whatever is specified by the FILTERCODE keyword):
Contains the input parameters from the configuration file.

HDU "FILTERS":
This HDU contains a binary table containing the names of the filters
and calculated magnitudes.  The table columns are:
   filter      string	    The filter name
   ewith       double	    The equivalent width of the passband
   L_lambda_to_L_nu	    double	   Conversion factor from
					   L_lambda to L_nu
					   (internal).
   L_lambda_effi   double   L_lambda,effective for camera i
   AB_magi	   double   AB magnitude for camera i
   L_lambda_eff_nonscatteri   double   L_lambda,effective for camera
				       i, without dust
   AB_mag_nonscatteri	      double   AB magnitude for camera i,
				       without dust
   					
HDU's "CAMERAi-BROADBAND":
These HDU's contain data cubes with the broadband images for the
appropriate camera.  The third dimension enumerates the filters in the
FILTERS HDU.  They also contain the following keywords:
SB_FACTR   double  Conversion factor from internal units to surface brightness
IMUNIT	   string  The unit of the pixel quantities

HDU's "CAMERAi-BROADBAND-NONSCATTER":
These HDU's contain data cubes with the broadband images without
dust.  Apart from this, they are identical to their dusty
counterparts.
